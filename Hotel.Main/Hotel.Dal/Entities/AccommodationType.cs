﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hotel.Dal.Enumerations;

namespace Hotel.Dal.Entities
{
    public class AccommodationType
    {
        public int? Id { get; set; }

        public AccommodationTypeEnum _AccommodationType { get; set; }

        private AccommodationType()
        {

        }

        public AccommodationType(AccommodationTypeEnum accommodationType, int? id = null)
        {
            _AccommodationType = accommodationType;
            Id = id;
        }
    }
}
