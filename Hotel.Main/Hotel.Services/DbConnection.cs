﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Hotel.Services
{
    public class DbConnection
    {
        protected MySqlConnection hotelConnection { get; private set; }

        public DbConnection()
        {
            hotelConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["hotelConnectionString"].ConnectionString);
        }
    }
}
