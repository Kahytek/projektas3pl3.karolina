﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hotel.Dal.Enumerations;
using Hotel.Dal.DataAnnotations;

namespace Hotel.ViewModels
{
    public class ReservationViewModel
    {
        public ReservationViewModel()
        {
            ParkingData = new _ParkingData();
        }

        public int? Id { get; set; }

        public DateTime CheckIn { get; set; }

        public DateTime CheckOut { get; set; }

        public List<_ReservationRooms> ReservationRooms { get; set; } //null kai pereinam i kita zingsi

        public _ParkingData ParkingData { get; set; }

        public bool IsGroup { get; set; }

        public class _ReservationRooms
        {
            public RoomTypeEnum RoomType { get; set; }

            public AccommodationTypeEnum AccommodationType { get; set; }

            public int Quantity { get; set; }

            public int? FullPrice { get; set; }
        }

        public class _ParkingData
        {
            public bool IsNeedParking { get; set; }
            
            public int ParkingSpaces { get; set; }
        }

        public ClientViewModel ClientViewModel { get; set; }

        public int Nights {
            get
            {
               return Convert.ToInt32((CheckOut - CheckIn).TotalDays - 1);
            }
        }

        public List<TempRoomAddViewModel> TempRooms { get; set; }

        public void MapLists()
        {
            ReservationRooms = new List<_ReservationRooms>();
            foreach (var tempRoom in TempRooms)
            {
                ReservationRooms.Add(new _ReservationRooms
                {
                    AccommodationType = (AccommodationTypeEnum)tempRoom.AccommodationTypeId,
                    RoomType = (RoomTypeEnum)tempRoom.RoomTypeId,
                    Quantity = tempRoom.Quantity
                });
            }
        }
    }
}
