CREATE DATABASE  IF NOT EXISTS `hotelnkkm` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `hotelnkkm`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: hotelnkkm
-- ------------------------------------------------------
-- Server version	5.7.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accommodationtypes`
--

DROP TABLE IF EXISTS `accommodationtypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accommodationtypes` (
  `Id` int(11) NOT NULL,
  `Type` int(11) DEFAULT NULL,
  `Price` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accommodationtypes`
--

LOCK TABLES `accommodationtypes` WRITE;
/*!40000 ALTER TABLE `accommodationtypes` DISABLE KEYS */;
INSERT INTO `accommodationtypes` VALUES (1,1,7),(2,2,30),(3,3,10);
/*!40000 ALTER TABLE `accommodationtypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservations`
--

DROP TABLE IF EXISTS `reservations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservations` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `RecordDate` datetime DEFAULT NULL,
  `Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservations`
--

LOCK TABLES `reservations` WRITE;
/*!40000 ALTER TABLE `reservations` DISABLE KEYS */;
INSERT INTO `reservations` VALUES (1,'2018-12-01 10:46:56',NULL),(2,'2018-12-01 10:48:05',NULL),(3,'2018-12-15 09:24:01',NULL),(4,'2018-12-15 09:43:56',NULL),(5,'2018-12-15 10:10:26',NULL),(6,'2018-12-15 10:10:41',NULL),(7,'2018-12-15 10:29:56',NULL),(8,'2018-12-15 10:34:52',NULL),(9,'2018-12-15 10:36:27',NULL),(10,'2018-12-15 10:39:20',NULL),(11,'2018-12-15 10:40:20',NULL),(12,'2018-12-15 10:40:54',NULL),(13,'2018-12-15 10:41:45',NULL),(14,'2018-12-15 10:42:24',NULL),(15,'2018-12-15 10:43:24',NULL),(16,'2018-12-15 10:44:03',NULL),(17,'2018-12-15 10:44:48',NULL),(18,'2018-12-15 10:45:31',NULL),(19,'2018-12-15 10:48:02',NULL),(20,'2018-12-15 10:50:03',NULL),(21,'2018-12-15 10:52:22',NULL),(22,'2018-12-15 10:54:14',NULL),(23,'2018-12-15 10:55:57',NULL),(24,'2018-12-15 10:56:43',NULL),(25,'2018-12-15 10:59:09',NULL),(26,'2018-12-15 10:59:38',NULL);
/*!40000 ALTER TABLE `reservations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms`
--

DROP TABLE IF EXISTS `rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rooms` (
  `RoomId` int(11) NOT NULL AUTO_INCREMENT,
  `RoomNumber` int(11) DEFAULT NULL,
  `RoomTypeId` int(11) DEFAULT NULL,
  `AccommodationTypeId` int(11) DEFAULT NULL,
  `AdditionalInfo` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`RoomId`),
  KEY `RoomTypeId` (`RoomTypeId`),
  KEY `AccommodationTypeId` (`AccommodationTypeId`),
  CONSTRAINT `rooms_ibfk_1` FOREIGN KEY (`RoomTypeId`) REFERENCES `roomtypes` (`RoomTypeId`),
  CONSTRAINT `rooms_ibfk_2` FOREIGN KEY (`AccommodationTypeId`) REFERENCES `accommodationtypes` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms`
--

LOCK TABLES `rooms` WRITE;
/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;
INSERT INTO `rooms` VALUES (1,111,1,1,NULL);
/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roomtypes`
--

DROP TABLE IF EXISTS `roomtypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roomtypes` (
  `RoomTypeId` int(11) NOT NULL,
  `Type` int(11) DEFAULT NULL,
  `PricePerNight` float DEFAULT NULL,
  PRIMARY KEY (`RoomTypeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roomtypes`
--

LOCK TABLES `roomtypes` WRITE;
/*!40000 ALTER TABLE `roomtypes` DISABLE KEYS */;
INSERT INTO `roomtypes` VALUES (1,1,25),(2,2,50),(3,3,15);
/*!40000 ALTER TABLE `roomtypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `temproomadding`
--

DROP TABLE IF EXISTS `temproomadding`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `temproomadding` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ReservationId` int(11) DEFAULT NULL,
  `RoomTypeId` int(11) DEFAULT NULL,
  `AccommodationTypeId` int(11) DEFAULT NULL,
  `Quantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `temproomadding`
--

LOCK TABLES `temproomadding` WRITE;
/*!40000 ALTER TABLE `temproomadding` DISABLE KEYS */;
INSERT INTO `temproomadding` VALUES (1,6,3,3,1),(2,7,3,3,2),(3,8,1,1,1),(4,9,1,1,1),(5,10,1,1,1),(6,12,1,1,1),(7,13,1,1,1),(8,14,2,2,1),(9,15,2,1,1),(10,16,1,1,2),(11,17,1,1,1),(12,18,2,1,1),(13,19,2,1,1),(14,20,2,1,1),(15,21,1,1,1),(16,22,1,1,1),(17,23,1,1,1),(18,24,1,1,1),(19,26,2,1,3),(20,26,1,3,2);
/*!40000 ALTER TABLE `temproomadding` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'hotelnkkm'
--

--
-- Dumping routines for database 'hotelnkkm'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-17 19:41:09
